//
//  LCARS_CalculatorAppDelegate.h
//  LCARS Calculator
//
//  Created by Danny Draper on 18/08/2010.
//  Copyright CedeSoft Ltd 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class POLY_CalculatorViewController;

@interface LCARS_CalculatorAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    //LCARS_CalculatorViewController *viewController;
	POLY_CalculatorViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet POLY_CalculatorViewController *viewController;

@end

